# Game Of Life in C++

This project is a kata performed in order to understand how to write unit tests with Qt, hexagonal architecture & lambdas

## Prerequisites

* C++11
* Qt framework, >= Qt5


## What we learnt

* Lambda in C++
* Unit tests in Qt
* How to create use cases
* Dependencies inversion


## What we should do, next time

* Dependencies injection
* Tests improvment
* templates: how to use either a callable or a function
