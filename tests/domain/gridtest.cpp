#include "../pumpkin-test-framework/pumpkintest.h"

#include <regex>
#include <algorithm>
#include <cctype>
#include <locale>

#include <domain/model/board.h>

#include "../utils.h"

#include "helpers/boardtesthelper.h"






class BoardTest: public PumpkinTest::AutoRegisteredTestFeature
{
    static GridTestHelper the_grid(std::string const& initial)
    {
        return GridTestHelper(initial);
    }
public:
    BoardTest(): PumpkinTest::AutoRegisteredTestFeature("Grid Unit Tests")
    {
       test("An empty grid should stay empty", []()
       {
            the_grid(R"(
                    . . .
                    . . .
                    . . .
                    )")
                    .after_a_cycle()
                    .should_be(R"(
                    . . .
                    . . .
                    . . .
                    )");
        });

        test("A cell with no neighbours should die", []()
        {
            the_grid(R"(
                    . . .
                    . o .
                    . . .
                    )")
                    .after_a_cycle()
                    .should_be(R"(
                    . . .
                    . . .
                    . . .
                    )");
        });


        test("An alive cell with 2 neighbours should stay alive", []()
        {
            the_grid(R"(
                    o o .
                    o . .
                    . . .
                    )")
                    .after_a_cycle()
                    .should_be(R"(
                    o o .
                    o o .
                    . . .
                    )");
        });

        test("A dead cell with 3 neighbours should spring", []()
        {
            the_grid(R"(
                    . . o
                    . . .
                    o . o
                    )")
                    .after_a_cycle()
                    .should_be(R"(
                    . . .
                    . o .
                    . . .
                    )");
        });


        test("A stable structure should not change", []()
        {
            the_grid(R"(
                    o o .
                    o o .
                    . . .
                    )")
                    .after_a_cycle()
                    .should_be(R"(
                    o o .
                    o o .
                    . . .
                    )");
        });

        test("A oscillator should be periodic", []()
        {
            the_grid(R"(
                    . . .
                    o o o
                    . . .
                    )")
                    .after_a_cycle()
                    .should_be(R"(
                    . o .
                    . o .
                    . o .
                    )")
                    .after_a_cycle()
                    .should_be(R"(
                    . . .
                    o o o
                    . . .
                    )");
        });
    }
};
REGISTER_PUMPKIN_TEST(BoardTest)
