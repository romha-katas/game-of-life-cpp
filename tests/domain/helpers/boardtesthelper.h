#ifndef BOARDTESTHELPER_H
#define BOARDTESTHELPER_H

#include "../pumpkin-test-framework/pumpkintest.h"

#include <domain/model/board.h>

#include "../utils.h"

class GridTestHelper {
private:
    Board currentGrid;
public:
    GridTestHelper(std::string const& initial): currentGrid(create_grid(initial))
    {}

    GridTestHelper& after_a_cycle()
    {
        currentGrid = currentGrid.next();
        return *this;
    }

    GridTestHelper& should_be(std::string const& expected)
    {
        Board expectedGrid {create_grid(expected)};

        PumpkinTest::Assertions::assertEquals( expectedGrid, currentGrid );

        return *this;
    }

};

#endif // BOARDTESTHELPER_H
