TEMPLATE = app
CONFIG -= qt
CONFIG -= app_bundle
CONFIG += console

#QMAKE_LFLAGS += --coverage -O0

QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
QMAKE_LFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0

QMAKE_POST_LINK = rm -f "*.gcda"

include(../Pumpkin-test-framework/pumpkin-test-framework.pri)

SOURCES += \
    celltest.cpp \
    gridtest.cpp \
    main.cpp

HEADERS += \
    helpers/boardsnapshottesthelper.h \
    helpers/boardtesthelper.h \
    utils.h



DEPENDPATH += $$PWD/../../app/
INCLUDEPATH += $$PWD/../../app/

SOURCES += \
    ../../app/domain/model/board.cpp \
    ../../app/domain/model/cell.cpp \
    ../../app/domain/model/geometry.cpp

HEADERS += \
    ../../app/domain/spi/alivecellinitializer.h \
    ../../app/domain/model/board.h \
    ../../app/domain/model/cell.h \
    ../../app/domain/model/geometry.h \
    ../../app/domain/model/point.h \
    ../../app/domain/model/state.h

