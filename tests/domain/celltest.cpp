#include "../pumpkin-test-framework/pumpkintest.h"

#include <random>

#include <domain/model/cell.h>

class CellTest: public PumpkinTest::AutoRegisteredTestFeature
{
private:
    Cell aliveCell()
    {
        return Cell(Point(0, 0), State::Alive);
    }

    Cell deadCell()
    {
        return Cell(Point(0, 0), State::Dead);
    }


    unsigned int any_number_except(std::initializer_list<unsigned int> const& exceptions)
    {
        static std::random_device rd;
        static std::default_random_engine e( rd() );


        std::vector<unsigned int> ex { exceptions };

        std::vector<int> choices(9);
        std::iota(choices.begin(), choices.end(), 0);

        choices.erase(std::remove_if(choices.begin(), choices.end(), [ex](unsigned int value) { return std::find(ex.begin(), ex.end(), value) != ex.end(); }),
                      choices.end());

       auto start = choices.begin();
        auto end = choices.end();
        std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
        std::advance(start, dis(e));
        return *start;

    }

public:
    CellTest(): PumpkinTest::AutoRegisteredTestFeature("Cell Unit Tests")
    {
        test("A dead cell should spring when it has 3 alive neighbours", [this]()
        {
            auto cell = deadCell();
            auto evolvedCell = cell.evolved(3);
            PumpkinTest::Assertions::assertTrue( cell.isDead() );
            PumpkinTest::Assertions::assertTrue( evolvedCell.isAlive() );
        });

        test("A dead cell should stay dead when it hasn't 3 alive neighbours", [this]()
        {
            auto cell = deadCell();
            auto evolvedCell = cell.evolved(any_number_except({3}));
            PumpkinTest::Assertions::assertTrue( cell.isDead() );
            PumpkinTest::Assertions::assertTrue( evolvedCell.isDead() );
        });

        test("An alive cell should stay alive when it has 2 alive neighbours", [this]()
        {
            auto cell = aliveCell();
            auto evolvedCell = cell.evolved(2);
            PumpkinTest::Assertions::assertTrue( evolvedCell.isAlive() );
        });

        test("An alive cell should stay alive when it has 3 alive neighbours", [this]()
        {
            auto cell = aliveCell();
            auto evolvedCell = cell.evolved(3);
            PumpkinTest::Assertions::assertTrue( evolvedCell.isAlive() );
        });

        test("An alive cell should die when it hasn't 2 or 3 alive neighbours", [this]()
        {
            auto cell = aliveCell();
            auto evolvedCell = cell.evolved(any_number_except({2, 3}));
            PumpkinTest::Assertions::assertTrue( evolvedCell.isDead() );
        });
    }
};
REGISTER_PUMPKIN_TEST(CellTest)
