HEADERS += \
    $$PWD/private/summarizable.h \
    $$PWD/pumpkintest.h \
    $$PWD/private/testsuite.h \
    $$PWD/private/test.h \
    $$PWD/private/summary.h \
    $$PWD/private/assertions.h \
    $$PWD/private/exceptions.h \
    $$PWD/private/autoregistration.h \
    $$PWD/private/autoregistrable.h \
    $$PWD/private/autoregistrationcampaign.h \
    $$PWD/private/factories.h \
    $$PWD/private/section.h

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

DISTFILES += \
    $$PWD/README.md
