#ifndef GOF_TESTS_UTILS_H
#define GOF_TESTS_UTILS_H

#include <domain/model/board.h>
#include <domain/services/boardcreator.h>

#include <sstream>
#include <regex>



/*inline std::ostream& operator<<(std::ostream& os, Geometry const& value)
{
   os << value.getWidth() << "/" << value.getHeight();
   return os;
}

inline std::ostream& operator<<(std::ostream& os, Point const& position)
{
   os << position.getX() << ";" << position.getY();
   return os;
}*/

inline std::ostream& operator<<(std::ostream& os, Cell const& value)
{
    os << value.getPosition().getX() << ";" << value.getPosition().getY() << ": ";
    if (value.isAlive())
        os << "Alive";
    else
        os << "Dead";
    return os;
}

inline std::ostream& operator<<(std::ostream& os, Board const& board)
{
    board.visit([&os](Cell const& value) {
        os << value << std::endl;
    });
    return os;
}


static inline std::string trimmed(std::string s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
    return s;
}

inline std::string trimIndent(std::string s)
{
    std::regex sep ("\n");
    std::sregex_token_iterator tokens(s.cbegin(), s.cend(), sep, -1);
    std::sregex_token_iterator end;

    std::vector<std::string> lines {tokens, end};

    std::stringstream ss;
    for (auto const& line: lines)
    {
        auto const l = trimmed(line);
        if (l != "")
            ss << l << "\n";
    }
    return ss.str();
}


struct GridSpecifications
{
    Geometry const size;
    std::list<Cell> const aliveCells;

    GridSpecifications(Geometry const& size, std::list<Cell> const& aliveCells): size(size), aliveCells(aliveCells){}

};

inline GridSpecifications createSpecs(std::string const& initial)
{
    std::regex sep ("\n");
    std::sregex_token_iterator tokens(initial.cbegin(), initial.cend(), sep, -1);
    std::sregex_token_iterator end;

    std::vector<std::string> lines {tokens, end};

    unsigned int linesCount {0};
    unsigned int columnsCount {0};
    std::list<Cell> aliveCells;
    for (auto const& l: lines)
    {
        std::string line = trimmed(l);
        if (line == "")
            continue;
        std::regex sep (" ");
        std::sregex_token_iterator tokens(line.cbegin(), line.cend(), sep, -1);
        std::vector<std::string> columns {tokens, end};
        unsigned int c {0};
        for (auto const& column: columns)
        {
            if (column == "")
                continue;
            if (column == "o")
                aliveCells.push_back(Cell(Point(linesCount, c), State::Alive));
            ++c;
        }
        columnsCount = std::max(columnsCount, c);
        ++linesCount;
    }

    return GridSpecifications(Geometry(linesCount, columnsCount), aliveCells);
}

inline Board create_grid(std::string const& initial)
{

    GridSpecifications specifications {createSpecs(initial)};
    return BoardCreator([specifications](Geometry const&) {return specifications.aliveCells;}) (specifications.size);
}

#endif // GOF_TESTS_UTILS_H
