#ifndef BOARDSNAPSHOTTESTHELPER_H
#define BOARDSNAPSHOTTESTHELPER_H

#include "../pumpkin-test-framework/pumpkintest.h"

#include <domain/board.h>
#include <infra/console/boardsnapshot.h>

#include "../utils.h"




class GridSnapshotTestHelper
{
private:
    Board board;
    BoardSnapshot snapshot;
public:

    GridSnapshotTestHelper(std::string const& initial): board(create_grid(trimmed(initial))), snapshot(board)
    {}

    GridSnapshotTestHelper& after_a_cycle()
    {
        board = board.next();
        return *this;
    }

    void should_display(std::string const& def)
    {
        auto const result= snapshot.snapshot();
        PumpkinTest::Assertions::assertEquals(trimIndent(def), trimIndent(result));
    }
};


#endif // BOARDSNAPSHOTTESTHELPER_H
