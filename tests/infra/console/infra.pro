TEMPLATE = app
CONFIG -= app_bundle
CONFIG += console

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


#QMAKE_LFLAGS += --coverage -O0

QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
QMAKE_LFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0

QMAKE_POST_LINK = rm -f "*.gcda"

include(../Pumpkin-test-framework/pumpkin-test-framework.pri)

SOURCES += \
    ../../app/infra/qt/boardrenderer.cpp \
    main.cpp \
    snapshottest.cpp


# Infra
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../app/infra/release/ -linfra
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../app/infra/debug/ -linfra
else:unix: LIBS += -L$$OUT_PWD/../../app/infra/ -linfra

# Domain
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../app/domain/release/ -ldomain
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../app/domain/debug/ -ldomain
else:unix: LIBS += -L$$OUT_PWD/../../app/domain/ -ldomain



INCLUDEPATH += $$PWD/../../app/
DEPENDPATH += $$PWD/../../app/

HEADERS += \
    ../../app/infra/qt/boardrenderer.h \
    helpers/boardsnapshottesthelper.h \
    helpers/boardtesthelper.h \
    utils.h

