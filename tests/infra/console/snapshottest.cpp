#include "../pumpkin-test-framework/pumpkintest.h"

#include <regex>
#include <algorithm>
#include <cctype>
#include <locale>


#include "helpers/boardsnapshottesthelper.h"

class GridSnapshotTest: public PumpkinTest::AutoRegisteredTestFeature
{
    static GridSnapshotTestHelper the_grid(std::string const& def)
    {
        return GridSnapshotTestHelper(def);
    }
public:
    GridSnapshotTest(): PumpkinTest::AutoRegisteredTestFeature("Grid Snapshot Unit Tests")
    {
       test("An empty grid should stay empty", []()
       {
            the_grid(R"(
                    . . .
                    . . .
                    . . .
                    )")
                    .should_display(R"(
                    . . .
                    . . .
                    . . .
                    )");
        });

       test("A snapshot should take the current state of a grid", []()
       {
            the_grid(R"(
                    . o .
                    . o .
                    . o .
                    )")
                    .after_a_cycle()
                    .should_display(R"(
                    . . .
                    o o o
                    . . .
                    )");
        });
    }
};
REGISTER_PUMPKIN_TEST(GridSnapshotTest)
