#include <QtTest>
#include <QCoreApplication>

#include <QWidget>
#include <QPainter>

#include <domain/spi/alivecellinitializer.h>
#include <domain/services/boardcreator.h>
#include <domain/model/board.h>

#include <infra/qt/boardproxy.h>
#include <infra/qt/boardrenderer.h>


int diff(QImage const& left, QImage const& right)
{
    auto pixelsCount = [](QSize const& s) { return s.width() * s.height(); };
    int diff =qAbs(pixelsCount(left.size()) - pixelsCount(right.size()));

    int const h = qMin(left.height(), right.height());
    int const w = qMin(left.width(), right.width());

    for (int x {0}; x != w; ++x)
        for (int y {0}; y != h; ++y)
            if (left.pixel(x, y) != right.pixel(x, y))
            {
                ++diff;
            }

    return diff;
}


QPixmap createExpected(std::initializer_list<QPoint> const& positions)
{
    QPixmap expected(200, 200);
    expected.fill(QPalette().color(QPalette::Window));
    QPainter painter(&expected);
    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::black);

    std::for_each(positions.begin(), positions.end(), [&painter](QPoint const& p)
    {
        painter.drawRect(QRect(p, QSize(20, 20)));
    });
    return expected;
}

QPixmap snapshot(QWidget* widget)
{
    QPixmap pixmap(widget->size());
    QPainter painter(&pixmap);

    widget->render(&painter);

    painter.end();

    return pixmap;
}

class InitializerStub: public AliveCellInitializer
{
private:
    std::list<Cell> aliveCells;
public:
    virtual std::list<Cell> operator()(Geometry const& /*size*/) const override {
        return aliveCells;
    }

    InitializerStub withCellAt(int x, int y) { aliveCells.push_back(Cell(Point(x, y), State::Alive)); return *this; }
};


class BoardRendererTestHelper
{
private:
    InitializerStub stub;
    CreateBoard* creator;
    EvolutingBoard* randomGame;
    QWidget* game;
public:
    BoardRendererTestHelper(): stub(), creator(new BoardCreator(stub))
    {
        Board board = (*creator)(Geometry(50, 50));
        randomGame = new BoardProxy(1000, board);
        game = new BoardRenderer(randomGame);
        game->resize(200, 200);
    }
};


class BoardRendererTest : public QObject
{
    Q_OBJECT

private:
    BoardRendererTestHelper given_a_board()
    {
        return BoardRendererTestHelper();
    }

public:
    BoardRendererTest();
    ~BoardRendererTest();

private slots:
     void should_create_empty_board()
     {
         Board board = BoardCreator(InitializerStub())(Geometry(50, 50));
         EvolutingBoard* randomGame = new BoardProxy(1000, board);
         QWidget* game = new BoardRenderer(randomGame);
         game->resize(200, 200);

         QPixmap result(200, 200);
         QPainter painter(&result);

         game->render(&painter);

         QPixmap expected(200, 200);
         expected.fill(game->palette().color(QPalette::Window));

         result.save("./result.png");
         expected.save("./expected.png");

         QVERIFY(expected.toImage() == result.toImage());
     }


     void should_respect_objects_lifecycle()
     {
         InitializerStub stub;
         auto* creator = new BoardCreator(stub);
         Board board = (*creator)(Geometry(50, 50));
         EvolutingBoard* randomGame = new BoardProxy(1000, board);
         QWidget* game = new BoardRenderer(randomGame);
         game->resize(200, 200);

         QPixmap result(200, 200);
         QPainter painter(&result);

         game->render(&painter);

         delete creator;
         randomGame->deleteLater();
         game->deleteLater();

         QApplication::processEvents();
     }


     void should_draw_cells()
     {
         InitializerStub stub;
         stub.withCellAt(5, 5);
         Board board = BoardCreator(stub)(Geometry(10, 10));
         EvolutingBoard* randomGame = new BoardProxy(1000, board);
         QWidget* game = new BoardRenderer(randomGame);
         game->resize(200, 200);

         QPixmap result = snapshot(game);
         QPixmap expected = createExpected({QPoint(100, 100)});

         QVERIFY(expected.toImage() == result.toImage());
     }

     void should_update_view_at_each_cycle()
     {
         InitializerStub stub;
         stub.withCellAt(5, 5);
         stub.withCellAt(5, 4);
         stub.withCellAt(5, 3);

         Board board = BoardCreator(stub)(Geometry(10, 10));

         EvolutingBoard* randomGame = new BoardProxy(1000, board);
         QWidget* game = new BoardRenderer(randomGame);
         game->resize(200, 200);

         QPixmap result = snapshot(game);
         QPixmap expected = createExpected(
                     {
                        QPoint(100, 100),
                        QPoint(100, 80),
                        QPoint(100, 60)
                     });

         result.save("./result.png");
         expected.save("./expected.png");

         QVERIFY(expected.toImage() == result.toImage());
     }


};

BoardRendererTest::BoardRendererTest()
{

}

BoardRendererTest::~BoardRendererTest()
{

}




QTEST_MAIN(BoardRendererTest)

#include "tst_boardrenderertest.moc"
