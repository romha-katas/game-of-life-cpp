QT += testlib

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG += c++11


CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
QMAKE_LFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0

QMAKE_POST_LINK = rm -f "*.gcda"

SOURCES +=  tst_boardrenderertest.cpp

DEPENDPATH += $$PWD/../../../app/
INCLUDEPATH += $$PWD/../../../app/

SOURCES += \
    ../../../app/infra/qt/boardproxy.cpp \
    ../../../app/infra/qt/boardrenderer.cpp \
    ../../../app/domain/model/board.cpp \
    ../../../app/domain/model/cell.cpp \
    ../../../app/domain/model/geometry.cpp

HEADERS += \
    ../../../app/domain/spi/alivecellinitializer.h \
    ../../../app/domain/model/board.h \
    ../../../app/domain/model/cell.h \
    ../../../app/domain/model/geometry.h \
    ../../../app/domain/model/point.h \
    ../../../app/domain/model/state.h \
    ../../../app/infra/qt/boardproxy.h \
    ../../../app/infra/qt/boardrenderer.h \
    ../../../app/infra/qt/evolutingboard.h
