#include "boardrenderer.h"

#include <QPainter>
#include <QTimer>

#include <QDebug>

#include "evolutingboard.h"


BoardRenderer::BoardRenderer(EvolutingBoard* board, QWidget* parent): QWidget(parent), board(board)
{
    connect(board, &EvolutingBoard::changed, this, [this]() { this->update(); } );
}


void BoardRenderer::paintEvent(QPaintEvent* /*event*/)
{

    Geometry const size { board->size() };
    QPainter painter(this);

    int const w = this->width() / size.getWidth();
    int const h = this->height() / size.getHeight();

    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::black);

    board->visit([w, h, &painter](Cell const& cell) {
        int x = cell.getPosition().getX() * w;
        int y = cell.getPosition().getY() * h;
        QRectF const r(x, y, w, h);
        if (cell.isAlive())
        {
            painter.drawRect(r);
        }
    });
}
