#ifndef EVOLUTINGBOARD_H
#define EVOLUTINGBOARD_H

#include <QObject>

#include "domain/model/geometry.h"

class Cell;

class EvolutingBoard: public QObject
{
    Q_OBJECT

protected:
    EvolutingBoard(QObject* parent=nullptr): QObject(parent) {}
public:
    virtual ~EvolutingBoard() {}
    virtual void visit(std::function<void(Cell const&)> const& visitor) const = 0;
    virtual Geometry size() const = 0;

signals:
    void changed();
};

#endif // EVOLUTINGBOARD_H
