#ifndef BOARDPROXY_H
#define BOARDPROXY_H

#include "evolutingboard.h"

#include <QTimer>

#include <domain/model/board.h>


class BoardProxy : public EvolutingBoard
{
private:
    QTimer* timer;
    Board board;
public:
    explicit BoardProxy(unsigned int interval, Board const& initialBoard, QObject *parent = nullptr);
    virtual ~BoardProxy();
    inline virtual void visit(std::function<void(Cell const&)> const& visitor) const override
    {
        board.visit(visitor);
    }

    inline Geometry size() const override { return this->board.getSize(); }
};

#endif // BOARDPROXY_H
