#ifndef BOARD_RENDERER_H
#define BOARD_RENDERER_H

#include <QWidget>

#include <domain/model/board.h>
#include <domain/model/geometry.h>

class EvolutingBoard;

class BoardRenderer: public QWidget
{
private:
    EvolutingBoard* board;

public:
    BoardRenderer(EvolutingBoard* board, QWidget* parent=nullptr);

   void paintEvent(QPaintEvent *event) override;
};

#endif // BOARD_RENDERER_H
