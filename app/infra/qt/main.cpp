#include <iostream>
#include <sstream>
#include <random>

#include <QApplication>

#include <domain/model/geometry.h>
#include <domain/spi/alivecellinitializer.h>
#include <domain/services/boardcreator.h>

#include "boardrenderer.h"
#include "boardproxy.h"

class RandomInitializer: public AliveCellInitializer
{
public:
    virtual std::list<Cell> operator()(Geometry const& size) const override {
        auto const cellMaxCount = size.getWidth() * size.getHeight();

        static std::random_device rd;
        std::default_random_engine e( rd() );

        std::uniform_int_distribution<> dis(0, cellMaxCount);
        int const cellCount = dis(e);

        auto gen = [&dis, &e, size](){
                           auto const p = dis(e);
                           auto const y = p / size.getWidth();
                           auto const x = p - (y * size.getWidth());

                           return Cell(Point(x, y), State::Alive);
                       };

        std::vector<Cell> points(cellCount, Cell(Point(0, 0), State::Alive));
        generate(begin(points), end(points), gen);

        return std::list<Cell>(points.begin(), points.end());
    }
};


int main(int argc, char** argv)
{


    QApplication app(argc, argv);

    Board board = BoardCreator(RandomInitializer())(Geometry(50, 50));
    EvolutingBoard* randomGame = new BoardProxy(1000, board);
    QWidget* game = new BoardRenderer(randomGame);
    game->show();

    return app.exec();


    return 0;
}
