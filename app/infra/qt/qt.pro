TEMPLATE = app
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11


SOURCES += \
        boardproxy.cpp \
        boardrenderer.cpp \
        main.cpp

HEADERS += \
    boardproxy.h \
    boardrenderer.h \
    evolutingboard.h

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../domain/release/ -ldomain
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../domain/debug/ -ldomain
else:unix: LIBS += -L$$OUT_PWD/../../domain/ -ldomain

INCLUDEPATH += $$PWD/../../
DEPENDPATH += $$PWD/../../
