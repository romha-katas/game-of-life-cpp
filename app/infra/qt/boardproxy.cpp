#include "boardproxy.h"

#include <QTimer>


BoardProxy::BoardProxy(unsigned int interval, Board const& initialBoard, QObject *parent)
    : EvolutingBoard(parent), timer(new QTimer(this)), board(initialBoard)
{
    timer->setInterval(interval);
    connect(timer, &QTimer::timeout, this, [this]() {
        board = board.next();
        emit changed();
    });
    timer->start();
}

BoardProxy::~BoardProxy()
{

}
