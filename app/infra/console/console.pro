TEMPLATE = lib
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

HEADERS += \
    boardsnapshot.h

SOURCES += \
    boardsnapshot.cpp


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../domain/release/ -ldomain
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../domain/debug/ -ldomain
else:unix: LIBS += -L$$OUT_PWD/../../domain/ -ldomain

INCLUDEPATH += $$PWD/../../
DEPENDPATH += $$PWD/../../
