#include "boardsnapshot.h"

#include <sstream>

#include <domain/model/board.h>

BoardSnapshot::BoardSnapshot(Board const& board): board(board)
{}


std::string BoardSnapshot::snapshot() const
{
    std::stringstream stream;
    int currentLine = 0;
    board.visit([&currentLine, &stream](Cell const& cell) {
        if (currentLine != cell.getPosition().getX()) {
            ++currentLine;
            stream << std::endl;
        }
        if (cell.isAlive())
            stream << "o ";
        else
            stream << ". ";
    });

    return stream.str();
}
