#ifndef BOARDSNAPSHOT_H
#define BOARDSNAPSHOT_H

#include <iostream>

class Board;

class BoardSnapshot
{
private:
    Board const& board;
public:
    BoardSnapshot(Board const& board);

    std::string snapshot() const;
};

#endif // BOARDSNAPSHOT_H
