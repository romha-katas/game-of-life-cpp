#ifndef ALIVECELLINITIALIZER_H
#define ALIVECELLINITIALIZER_H

#include "../model/cell.h"
#include "../model/geometry.h"

#include <list>

class AliveCellInitializer
{
public:
    virtual std::list<Cell> operator()(Geometry const& size) const = 0;
};

#endif // ALIVECELLINITIALIZER_H
