#ifndef BOARDCREATOR_H
#define BOARDCREATOR_H

#include "../api/createboard.h"

class AliveCellInitializer;

template<class T> class BoardCreator: public CreateBoard
{
private:
    T const& initializer;
public:
    explicit BoardCreator(T const& initializer): CreateBoard(), initializer(initializer) {}

    virtual Board operator()(Geometry const& size) const override
    {
        return create(size, initializer(size));
    }

    virtual ~BoardCreator() {}
};

#endif // BOARDCREATOR_H
