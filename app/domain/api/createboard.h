#ifndef CREATEBOARD_H
#define CREATEBOARD_H


#include "../model/board.h"

class CreateBoard
{
protected:
    inline Board create(Geometry const& size, std::list<Cell> const& aliveCells) const
    {
        return Board(size, aliveCells);
    }
public:
    virtual Board operator()(Geometry const& size) const = 0;
    virtual ~CreateBoard() {}
};

#endif // CREATEBOARD_H
