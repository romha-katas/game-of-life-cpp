TEMPLATE = lib
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt


SOURCES += \
        model/board.cpp \
        model/cell.cpp \
        services/boardcreator.cpp \
        model/geometry.cpp

HEADERS += \
    services/boardcreator.h \
    spi/alivecellinitializer.h \
    model/board.h \
    model/cell.h \
    api/createboard.h \
    model/geometry.h \
    model/point.h \
    model/state.h

