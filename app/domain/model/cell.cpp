#include "cell.h"

Point Cell::getPosition() const
{
    return position;
}


Cell::Cell(Point const& position, State initialState): position(position), state(initialState)
{
}

bool Cell::operator==(Cell const& other)  const
{
    return this->position == other.position && this->state == other.state;
}

Cell Cell::evolved(unsigned int neighbours) const
{
    if (state == State::Dead)
    {
        return Cell(position, evolveFromDead(neighbours));
    }

    return Cell(position, evolveFromAlive(neighbours));
}

State Cell::evolveFromDead(unsigned int neighbours) const
{
    if (neighbours == 3)
    {
        return State::Alive;
    }

    return State::Dead;
}

State Cell::evolveFromAlive(unsigned int neighbours) const
{
    if (neighbours == 3 || neighbours == 2)
    {
        return State::Alive;
    }

    return State::Dead;
}
