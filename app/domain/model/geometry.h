#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <functional>
#include <list>

class Point;

class Geometry
{
private:
    bool inBounds(Point const& point) const;
    unsigned int width;
    unsigned int height;
public:
    Geometry(unsigned int width, unsigned int height);
    Geometry(Geometry const& other) = default;

    void traverse(std::function<void(Point const&)> const& callback) const;

    std::list<Point> contiguousOf(Point const& center) const;

    bool operator==(Geometry const& other) const;


    inline unsigned int getWidth() const { return width; };
    inline unsigned int getHeight() const { return height; };
};

#endif // GEOMETRY_H
