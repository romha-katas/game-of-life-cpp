#ifndef CELL_H
#define CELL_H


#include "point.h"
#include "state.h"

class Cell
{
private:
    Point position;
    State state;

    State evolveFromDead(unsigned int neighbours) const;
    State evolveFromAlive(unsigned int neighbours) const;
public:
    Cell(Point const& position, State state);
    Cell(Cell const& other) = default;
    Cell& operator=(Cell const& other) = default;


    bool operator==(Cell const& other)  const;

    inline bool isAlive() const { return state == State::Alive; }
    inline bool isDead() const { return state == State::Dead; }

    Cell evolved(unsigned int neighbours) const;
    Point getPosition() const;
};

#endif // CELL_H
