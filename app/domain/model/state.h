#ifndef STATE_H
#define STATE_H

enum State {
    Dead = 0,
    Alive = 1
};

#endif // STATE_H
