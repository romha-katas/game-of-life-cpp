#ifndef POINT_H
#define POINT_H


struct Offset
{
    int const dx;
    int const dy;
    Offset(int dx, int const dy): dx(dx), dy(dy) {}

};


class Point
{
private:
    int x;
    int y;

public:
    Point(int x, int const y): x(x), y(y){}
    Point(Point const& other) = default;
    Point& operator=(Point const& other) = default;

    inline Point operator+(Offset const& offset) const {
        return Point(x + offset.dx, y + offset.dy);
    }

    bool operator==(Point const& other) const
    {
        return this->x == other.x && this->y == other.y;
    }

    int getX() const { return x; }
    int getY() const { return y; }
};

#endif // POINT_H
