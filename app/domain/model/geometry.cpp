#include "geometry.h"

#include "point.h"


template<typename T> std::list<T> filter(std::list<T> const& items, std::function<bool(T const&)> const& predicate) {
    std::list<T> result;
    std::remove_copy_if(items.begin(), items.end(), result.begin(), predicate);
    return result;
}



Geometry::Geometry(unsigned int width, unsigned int height): width(width), height(height)
{}

bool Geometry::inBounds(Point const& point) const
{
    auto const x = point.getX();
    auto const y = point.getY();

    return x >= 0 && x < (int)width && y >= 0 && y < (int)height;

}


void Geometry::traverse(std::function<void(Point const&)> const& callback) const
{
    for (int l {0}; l != (int)width; ++l)
    {
        for (int c {0}; c != (int)height; ++c)
        {
            callback({l, c});
        }
    }
}

std::list<Point> Geometry::contiguousOf(Point const& center) const
{
    std::list<Point> all ({
                center + Offset(-1, -1),
                center + Offset(0, -1),
                center + Offset(1, -1),
                center + Offset(-1, 0),
                center + Offset(1, 0),
                center + Offset(-1, 1),
                center + Offset(0, 1),
                center + Offset(1, 1)
    });
    all.remove_if([center](Point const& point) { return point == center; });
    all.remove_if([this](Point const& point) { return !inBounds(point); });


    return all;
}


bool Geometry::operator==(Geometry const& other) const
{
    return this->width == other.width && this->height == other.height;
}
