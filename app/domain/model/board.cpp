#include "board.h"

#include <iostream>


static bool contains(std::list<Cell> const& aliveCells, Point const& searched)
{
    return std::find_if(aliveCells.begin(), aliveCells.end(), [searched](Cell const& cell){
        return searched == cell.getPosition();
    }) != aliveCells.end();
}


Board::Board(Geometry const& size, std::list<Cell> const& aliveCells): size(size), cells()
{
    this->size.traverse(
                [this, aliveCells](Point const& position) {
                    if (contains(aliveCells, position))
                        cells.push_back(Cell(position, State::Alive));
                    else
                        cells.push_back(Cell(position, State::Dead));
                });
}

Board &Board::operator=(const Board &other) {
    this->size = other.size;
    this->cells.clear();
    this->cells = other.cells;
    return *this;
}

unsigned int Board::neighboursOf(Cell const& square) const {
    auto allNeighbours = this->size.contiguousOf(square.getPosition());
    auto count = std::count_if(allNeighbours.begin(),
                         allNeighbours.end(),
                         [this](Point const& position) { return this->at(position).isAlive(); });
    return count;
};


Board Board::next() const
{
    std::list<Cell> evolvedCells;
    this->visit([&evolvedCells, this](Cell const& square)
    {
        auto newCell = square.evolved(neighboursOf(square));
        if (newCell.isAlive())
            evolvedCells.push_back(newCell);
    });

    return Board(size, evolvedCells);
}

Cell Board::at(Point const& position) const
{
    auto square = std::find_if(cells.begin(), cells.end(), [position](Cell const& square) { return square.getPosition() == position; });
    if (square == cells.end())
        return Cell(position, State::Dead);
    return *square;
}


bool Board::operator==(Board const& other) const
{
    return this->size == other.size && this->cells == other.cells;
}

void Board::visit(std::function<void(Cell const&)> const& visitor) const
{
    for (auto const& square: this->cells)
    {
        visitor(square);
    }
}
