#ifndef BOARD_H
#define BOARD_H

#include <list>

#include "geometry.h"
#include "point.h"


#include "cell.h"

class Board
{
private:
    Geometry size;
    std::list<Cell> cells;

    Cell at(Point const& position) const;
    unsigned int neighboursOf(Cell const& square) const;


    Board(Geometry const& size, std::list<Cell> const& aliveCells);

public:    
    Board(Board const& other) = default;
    Board& operator= (Board const& other);

    Board next() const;

    bool operator==(Board const& other) const;

    void visit(std::function<void(Cell const&)> const& visitor) const;

    inline Geometry getSize() const { return size; }

    friend class CreateBoard;
};

#endif // BOARD_H
