#ifndef CELLINITIALIZER_H
#define CELLINITIALIZER_H

#include <list>

#include "geometry.h"
#include "point.h"

class CellInitializer
{
public:
    virtual std::list<Point> operator()(Geometry const& size) const = 0;
};

#endif // CELLINITIALIZER_H
